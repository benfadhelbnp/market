package com.bnp.exercice;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Main {

	private BigDecimal totalPrice = new BigDecimal("0.00");
	private BigDecimal specialOffer = new BigDecimal("0.00");

	public static void main(String[] args) {
		Product apple = new Product(ItemEnum.APPLE, new BigDecimal("0.20"));
		Product orange = new Product(ItemEnum.ORANGE, new BigDecimal("0.50"));
		Product waterMelon = new Product(ItemEnum.WATERMELON, new BigDecimal("0.80"));

		Map<Product, Integer> basket = new HashMap<Product, Integer>();
		basket.put(apple, 4);
		basket.put(orange, 3);
		basket.put(waterMelon, 5);
		Main main = new Main();
		System.out.println(main.getBasketAmount(basket));
	}

	public BigDecimal getBasketAmount(Map<Product, Integer> basket) {
		StoreService storeService = (basketList) -> {
			basketList.forEach((product, quantity) -> {
				switch (product.getItem()) {
				case APPLE:
					specialOffer = new BigDecimal("0.50");
					specialOffer = specialOffer.multiply(new BigDecimal(quantity));
					totalPrice = totalPrice.add(quantity >= 2 ? product.getPrice().multiply(specialOffer)
							: product.getPrice().multiply(new BigDecimal(quantity)));
					break;
				case WATERMELON:
					specialOffer = new BigDecimal(2 / 3d);
					specialOffer = specialOffer.multiply(new BigDecimal(quantity)).setScale(0, BigDecimal.ROUND_UP);
					totalPrice = totalPrice.add(quantity >= 3 ? product.getPrice().multiply(specialOffer)
							: product.getPrice().multiply(new BigDecimal(quantity)));
					break;
				case ORANGE:
					totalPrice = totalPrice.add(product.getPrice().multiply(new BigDecimal(quantity)));
					break;
				default:
					break;
				}
			});
			return totalPrice.setScale(2);
		};

		return storeService.buy(basket);
	}
}
