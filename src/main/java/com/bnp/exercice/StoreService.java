package com.bnp.exercice;

import java.math.BigDecimal;
import java.util.Map;

@FunctionalInterface
public interface StoreService {

	public BigDecimal buy(Map<Product, Integer> basket);

}
