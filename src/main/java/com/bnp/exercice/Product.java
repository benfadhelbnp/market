package com.bnp.exercice;

import java.math.BigDecimal;

public class Product {
	ItemEnum item;
	BigDecimal price;

	/**
	 * @param item
	 * @param price
	 */
	public Product(ItemEnum item, BigDecimal price) {
		super();
		this.item = item;
		this.price = price;
	}

	/**
	 * @return the item
	 */
	public ItemEnum getItem() {
		return item;
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}
}
